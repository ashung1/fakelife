package ashung.fakelife.infrastructure;

public interface DataRepository<T> {
   void store(T t);

   T get(String key);

   void delete(String key);
}
