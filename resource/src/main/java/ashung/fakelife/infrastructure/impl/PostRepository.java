package ashung.fakelife.infrastructure.impl;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;

import ashung.fakelife.infrastructure.DataRepository;
import ashung.fakelife.model.PostDto;

public class PostRepository implements DataRepository<PostDto> {
   private Map<String, PostDto> postStore = new HashMap<>();
   private Map<String, Set<String>> userPosts = new HashMap<>();

   public Set<String> getAllPosts(String userId) {
      return userPosts.containsKey(userId) ? userPosts.get(userId) : new HashSet<String>();
   }

   @Override
   public void store(PostDto dto) {
      postStore.put(dto.getId(), dto);
      if (!userPosts.containsKey(dto.getUserId()))
         userPosts.put(dto.getUserId(), ConcurrentHashMap.newKeySet());
      userPosts.get(dto.getUserId()).add(dto.getId());
   }

   @Override
   public PostDto get(String id) {
      return postStore.get(id);
   }

   @Override
   public void delete(String id) {
      postStore.remove(id);
   }
}
