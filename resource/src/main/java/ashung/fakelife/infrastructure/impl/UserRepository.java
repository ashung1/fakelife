package ashung.fakelife.infrastructure.impl;

import java.util.HashMap;
import java.util.Map;

import ashung.fakelife.infrastructure.DataRepository;
import ashung.fakelife.model.UserDto;

public class UserRepository implements DataRepository<UserDto> {

   private static Map<String, UserDto> userStore = new HashMap<>();

   @Override
   public void store(UserDto t) {
      userStore.put(t.getId(), t);
   }

   @Override
   public UserDto get(String key) {
      return userStore.get(key);
   }

   @Override
   public void delete(String key) {
      userStore.remove(key);
   }

}
