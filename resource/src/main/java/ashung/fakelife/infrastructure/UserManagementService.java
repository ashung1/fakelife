package ashung.fakelife.infrastructure;

import java.util.UUID;

import ashung.fakelife.infrastructure.impl.UserRepository;
import ashung.fakelife.model.UserDto;

public class UserManagementService {
   private UserRepository userRepository = new UserRepository();

   public UserDto register(UserDto dto) {
      dto.setId(UUID.randomUUID().toString());
      userRepository.store(dto);
      return findUser(dto.getId());
   }

   public UserDto findUser(String userId) {
      return userRepository.get(userId);
   }

   public void updateUser(UserDto dto) {
      userRepository.store(dto);
   }
}
