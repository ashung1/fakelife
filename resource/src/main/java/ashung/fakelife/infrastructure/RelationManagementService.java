package ashung.fakelife.infrastructure;

import javax.annotation.Resource;

import ashung.fakelife.model.RelationDto;
import ashung.fakelife.model.UserDto;

public class RelationManagementService {
   @Resource(name = "userManagementService")
   private UserManagementService userManagementService;

   public RelationManagementService()
   {}

   RelationManagementService(UserManagementService userManagementService) {
      this.userManagementService = userManagementService;
   }

   public RelationDto[] findRelations(String userId) {
      UserDto user = userManagementService.findUser(userId);
      return user != null ? user.getRelations().toArray(new RelationDto[0]) : new RelationDto[0];
   }

   public void newRelation(RelationDto dto) {
      UserDto userDto = userManagementService.findUser(dto.getFromId());
      if (userDto == null)
         return;
      userDto.getRelations().add(dto);
      userManagementService.updateUser(userDto);
   }

}
