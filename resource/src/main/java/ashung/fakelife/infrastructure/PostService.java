package ashung.fakelife.infrastructure;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.Set;
import java.util.UUID;

import javax.annotation.Resource;
import javax.ws.rs.NotFoundException;

import ashung.fakelife.infrastructure.impl.PostRepository;
import ashung.fakelife.model.PostDto;
import ashung.fakelife.model.RelationDto;
import ashung.fakelife.model.ReplyMessageDto;

public class PostService {
   @Resource(name = "relationManagementService")
   private RelationManagementService relationManagementService;
   private PostRepository postRepository = new PostRepository();
   private Comparator<PostDto> comparator = new Comparator<PostDto>() {
      @Override
      public int compare(PostDto o1, PostDto o2) {
         return Long.compare(o1.getTimestamp(), o2.getTimestamp());
      }
   };

   public PostService()
   {}
   
   PostService(RelationManagementService relationManagementService)
   {
      this.relationManagementService = relationManagementService;
   }
   public PostDto[] getPosts(String userId) {
      Set<String> allPosts = postRepository.getAllPosts(userId);
      RelationDto[] findRelations = relationManagementService.findRelations(userId);
      for (RelationDto relation : findRelations)
         allPosts.addAll(postRepository.getAllPosts(relation.getTargetId()));
      return retrieveAllPosts(allPosts);
   }

   private PostDto[] retrieveAllPosts(Set<String> postIds) {
      if (postIds == null)
         return new PostDto[0];
      List<PostDto> allPosts = new ArrayList<>();
      for (String postId : postIds)
         allPosts.add(postRepository.get(postId));
      allPosts.sort(comparator);
      return allPosts.toArray(new PostDto[0]);
   }

   public void newPost(PostDto dto) {
      dto.setId(UUID.randomUUID().toString());
      dto.setTimestamp(System.currentTimeMillis());
      postRepository.store(dto);
   }

   public void updatePost(PostDto dto) {
      postRepository.store(dto);
   }

   public void removePost(String id) {
      postRepository.delete(id);
   }

   public void addLike(String userId, String postId)
   {
      PostDto postDto = postRepository.get(postId);
      if (postDto != null)
      {
         postDto.getLike().add(userId);
         updatePost(postDto);
      }
   }

   public void sharePost(String userId, String postId)
   {
      PostDto postDto = postRepository.get(postId);
      if (postDto == null)
         throw new NotFoundException();
      PostDto newPost = new PostDto();
      newPost.setStringContent(postDto.getStringContent());
      newPost.setUserId(userId);
      newPost(newPost);
   }

   public void addReply(ReplyMessageDto replyMessageDto, String postId)
   {
      PostDto postDto = postRepository.get(postId);
      if (postDto == null)
         throw new NotFoundException();
      replyMessageDto.setTimestamp(System.currentTimeMillis());
      postDto.getReplies().add(replyMessageDto);
      updatePost(postDto);
   }

}
