package ashung.fakelife.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Scope;

import ashung.fakelife.infrastructure.PostService;
import ashung.fakelife.infrastructure.RelationManagementService;
import ashung.fakelife.infrastructure.UserManagementService;

@Configuration
public class ResourceConfig {
   @Bean(name = "postService")
   @Scope("singleton")
   public PostService postServiceProvider() {
      return new PostService();
   }

   @Bean(name = "relationManagementService")
   @Scope("singleton")
   public RelationManagementService relationManagementServiceProvider() {
      return new RelationManagementService();
   }
   
   @Bean(name = "userManagementService")
   @Scope("singleton")
   public UserManagementService userManagementServiceProvider() {
      return new UserManagementService();
   }
   
}
