package ashung.fakelife.config;

import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;

@Configuration
@EnableWebSecurity
public class PostsSecurityConfiguration extends WebSecurityConfigurerAdapter {

   @Override
   protected void configure(HttpSecurity http) throws Exception {
      http.authorizeRequests().antMatchers("/").permitAll().antMatchers("/posts")
         .hasAnyRole("ADMIN", "USER").anyRequest().authenticated().and().logout().permitAll();
      http.httpBasic();
      http.csrf().disable();

   }

   @Override
   public void configure(AuthenticationManagerBuilder authenticationMgr) throws Exception {
      authenticationMgr.inMemoryAuthentication().withUser("admin").password("pass").authorities("ROLE_ADMIN");
   }
}
