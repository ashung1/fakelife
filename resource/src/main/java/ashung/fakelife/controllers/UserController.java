package ashung.fakelife.controllers;

import javax.annotation.Resource;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import ashung.fakelife.infrastructure.UserManagementService;
import ashung.fakelife.model.UserDto;

@RestController
public class UserController {
   @Resource(name = "userManagementService")
   private UserManagementService userManagementService;

   @GetMapping(value = "/user/{id}", produces = "application/json")
   public UserDto get(@PathVariable("id") String id) {
      return userManagementService.findUser(id);
   }

   @PostMapping(value = "/user", consumes = "application/json")
   public UserDto post(@RequestBody UserDto userDto) {
      return userManagementService.register(userDto);
   }
}
