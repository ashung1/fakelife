package ashung.fakelife.controllers;

import javax.annotation.Resource;

import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import ashung.fakelife.infrastructure.PostService;
import ashung.fakelife.model.PatchPostDto;
import ashung.fakelife.model.PostDto;
import ashung.fakelife.model.ReplyMessageDto;
import ashung.fakelife.model.SharePostDto;

@RestController
public class PostController {

   @Resource(name = "postService")
   private PostService postService;

   @GetMapping(value = "/allposts/{userId}", produces = "application/json")
   public PostDto[] get(@PathVariable("userId") String userId) {
      return postService.getPosts(userId);
   }

   @PostMapping(value = "/posts", consumes = "application/json")
   public void post(@RequestBody PostDto postDto) {
      postService.newPost(postDto);
   }

   @PatchMapping(value = "/posts/{postId}", consumes = "application/json")
   public void patch(@RequestBody PatchPostDto patchPostDto, @PathVariable("postId") String postId) {
      if (patchPostDto.isLike())
         postService.addLike(patchPostDto.getUserId(), postId);
      else
         postService.addReply(new ReplyMessageDto(patchPostDto.getUserId(), patchPostDto.getReplyMessage()), postId);
   }

   @PostMapping(value = "/share", consumes = "application/json")
   public void share(@RequestBody SharePostDto sharePostDto) {
      postService.sharePost(sharePostDto.getUserId(), sharePostDto.getPostId());
   }

   @DeleteMapping(value = "/posts/{id}")
   public void post(@PathVariable("id") String id) {
      postService.removePost(id);
   }
}