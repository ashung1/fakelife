package ashung.fakelife.controllers;

import javax.annotation.Resource;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import ashung.fakelife.infrastructure.RelationManagementService;
import ashung.fakelife.model.RelationDto;

@RestController
public class RelationController {

   @Resource(name = "relationManagementService")
   private RelationManagementService relationManagementService;

   @GetMapping(value = "/relation/{id}", produces = "application/json")
   public RelationDto[] get(@PathVariable("id") String id) {
      return relationManagementService.findRelations(id);
   }

   @PostMapping(value = "/relation", consumes = "application/json")
   public void post(@RequestBody RelationDto relationDto) {
      relationManagementService.newRelation(relationDto);
   }

}
