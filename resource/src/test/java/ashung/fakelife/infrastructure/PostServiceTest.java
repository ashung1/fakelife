package ashung.fakelife.infrastructure;

import static org.junit.Assert.assertTrue;

import org.junit.Test;

import ashung.fakelife.model.IdentityDto;
import ashung.fakelife.model.PostDto;
import ashung.fakelife.model.RelationDto;
import ashung.fakelife.model.ReplyMessageDto;
import ashung.fakelife.model.UserDto;

public class PostServiceTest {

   private UserManagementService UserManagementService = new UserManagementService();
   private RelationManagementService relationManagementService = new RelationManagementService(UserManagementService);
   private PostService service = new PostService(relationManagementService);
   private UserDto userDto1 = createUser("user1");
   private UserDto userDto2 = createUser("user2");

   @Test
   public void postServiceTest() {
      //Post test
      PostDto[] posts = service.getPosts("123");
      assertTrue(posts.length == 0);
      posts = service.getPosts(userDto1.getId());
      assertTrue(posts.length == 0);
      newPost(userDto1.getId(), "Test post 1");
      posts = service.getPosts(userDto1.getId());
      assertTrue(posts.length == 1);

      //Follow test
      addRelation(userDto2.getId(), userDto1.getId());
      posts = service.getPosts(userDto2.getId());
      assertTrue(posts.length == 1);
      addRelation(userDto1.getId(), userDto2.getId());
      newPost(userDto2.getId(), "Test post 2");
      posts = service.getPosts(userDto2.getId());
      assertTrue(posts.length == 2);
      posts = service.getPosts(userDto1.getId());
      assertTrue(posts.length == 2);

      //Like test
      posts = service.getPosts(userDto1.getId());
      service.addLike(userDto1.getId(), posts[0].getId());
      posts = service.getPosts(userDto1.getId());
      assertTrue(posts[0].getLike().size() == 1);

      //Reply test
      posts = service.getPosts(userDto2.getId());
      ReplyMessageDto replyMessageDto = new ReplyMessageDto();
      replyMessageDto.setText("Test Reply");
      replyMessageDto.setUserId(userDto1.getId());
      service.addReply(replyMessageDto, posts[0].getId());
      posts = service.getPosts(userDto2.getId());
      assertTrue(posts[0].getReplies().size() == 1);
      assertTrue(posts[0].getReplies().iterator().next().getUserId().equals(userDto1.getId()));
      
      //Share Test
      int user2PostSize = posts.length;
      posts = service.getPosts(userDto1.getId());
      PostDto targetPost = null;
      for (PostDto post : posts)
      {
         if (!post.getUserId().equals(userDto2.getId()))
         {
            targetPost = post;
            break;
         }
      }
      service.sharePost(userDto2.getId(), targetPost.getId());
      posts = service.getPosts(userDto2.getId());
      assertTrue(posts.length == user2PostSize + 1);
      for (PostDto post : posts)
      {
         if (!post.getUserId().equals(targetPost.getUserId()) && post.getStringContent().equals(targetPost.getStringContent()))
            return;
      }
      assertTrue(false); //Should not come to here
   }

   private void newPost(String userId, String postContent) {
      PostDto postDto = new PostDto();
      postDto.setStringContent(postContent);
      postDto.setUserId(userId);
      service.newPost(postDto);
   }

   private UserDto createUser(String name) {
      UserDto userDto = new UserDto();
      userDto.setDisplayName(name);
      IdentityDto identityDto = new IdentityDto();
      identityDto.setName(name);
      identityDto.setId("1234");
      userDto.setIdentity(identityDto);
      userDto = UserManagementService.register(userDto);
      return userDto;
   }

   private void addRelation(String from, String to) {
      RelationDto relationDto = new RelationDto();
      relationDto.setFromId(from);
      relationDto.setTargetId(to);
      relationManagementService.newRelation(relationDto);
   }

}
