package ashung.fakelife.model;

public class PatchPostDto {
   private String userId;
   private String replyMessage;
   private boolean isLike;

   public String getUserId() {
      return userId;
   }

   public void setUserId(String userId) {
      this.userId = userId;
   }

   public String getReplyMessage() {
      return replyMessage;
   }

   public void setReplyMessage(String replyMessage) {
      this.replyMessage = replyMessage;
   }

   public boolean isLike() {
      return isLike;
   }

   public void setLike(boolean isLike) {
      this.isLike = isLike;
   }

}
