package ashung.fakelife.model;

import java.util.Collections;
import java.util.HashSet;
import java.util.Set;

public class UserDto {

   private String id;
   private String displayName;
   private IdentityDto identity;
   private Set<RelationDto> relations = new HashSet<>();

   public String getId() {
      return id;
   }

   public void setId(String id) {
      this.id = id;
   }

   public String getDisplayName() {
      return displayName;
   }

   public void setDisplayName(String displayName) {
      this.displayName = displayName;
   }

   public IdentityDto getIdentity() {
      return identity;
   }

   public void setIdentity(IdentityDto identity) {
      this.identity = identity;
   }

   public Set<RelationDto> getRelations() {
      return relations == null? Collections.emptySet() : relations;
   }

   public void setRelations(Set<RelationDto> relations) {
      this.relations = relations;
   }

   public void addRelation(RelationDto relationDto)
   {
      if (relationDto == null)
         return ;
      if (relations == null)
         relations = new HashSet<>();
      relations.add(relationDto);
   }


   @Override
   public String toString() {
      return "UserDto [id=" + id + ", displayName=" + displayName + ", identity=" + identity + "]";
   }

}