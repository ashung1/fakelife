package ashung.fakelife.model;

import java.util.Collections;
import java.util.HashSet;
import java.util.Set;

public class PostDto {
   private String id;
   private long timestamp;
   private String stringContent;
   private String userId;
   private Set<String> like = new HashSet<>();
   private Set<ReplyMessageDto> replies = new HashSet<>();
   
   public String getId() {
      return id;
   }

   public void setId(String id) {
      this.id = id;
   }

   public long getTimestamp() {
      return timestamp;
   }

   public void setTimestamp(long timestamp) {
      this.timestamp = timestamp;
   }

   public String getStringContent() {
      return stringContent;
   }

   public void setStringContent(String stringContent) {
      this.stringContent = stringContent;
   }

   public String getUserId() {
      return userId;
   }

   public void setUserId(String userId) {
      this.userId = userId;
   }

   public Set<String> getLike() {
      return like == null? Collections.emptySet() : like;
   }

   public void setLike(Set<String> like) {
      this.like = like;
   }

   public Set<ReplyMessageDto> getReplies() {
      return replies == null? Collections.emptySet() : replies;
   }

   public void setReplies(Set<ReplyMessageDto> replies) {
      this.replies = replies;
   }

}
