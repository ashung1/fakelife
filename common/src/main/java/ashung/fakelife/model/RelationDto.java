package ashung.fakelife.model;

public class RelationDto {

   public enum RelationType {
      Follow, Friend
   }

   private String fromId;
   private String targetId;
   private RelationType type;

   public String getFromId() {
      return fromId;
   }

   public void setFromId(String fromId) {
      this.fromId = fromId;
   }

   public String getTargetId() {
      return targetId;
   }

   public void setTargetId(String targetId) {
      this.targetId = targetId;
   }

   public RelationType getType() {
      return type;
   }

   public void setType(RelationType type) {
      this.type = type;
   }

   @Override
   public int hashCode() {
      return targetId.hashCode();
   }

   @Override
   public boolean equals(Object another) {
      if (another instanceof RelationDto) {
         return targetId.equals(((RelationDto) another).getTargetId());
      }
      return false;
   }
}
