package ashung.fakelife.model;

public class IdentityDto {
   private String id;
   private String name;

   public String getId() {
      return id;
   }

   public void setId(String id) {
      this.id = id;
   }

   public String getName() {
      return name;
   }

   public void setName(String name) {
      this.name = name;
   }

   @Override
   public String toString() {
      return "Identity [id=" + id + ", name=" + name + "]";
   }
}
