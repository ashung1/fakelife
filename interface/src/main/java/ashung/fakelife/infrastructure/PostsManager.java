package ashung.fakelife.infrastructure;

import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.http.client.HttpComponentsClientHttpRequestFactory;
import org.springframework.web.client.RestTemplate;

import ashung.fakelife.model.PatchPostDto;
import ashung.fakelife.model.PostDto;
import ashung.fakelife.model.SharePostDto;

public class PostsManager {

   public PostDto[] getPosts(String authorization, String id) {
      RestTemplate restTemplate = new RestTemplate();
      String url = "http://localhost:8080/allposts/" + id;
      HttpHeaders headers1 = new HttpHeaders();
      headers1.add("Authorization", authorization);
      HttpEntity<String> entity = new HttpEntity<>(headers1);
      ResponseEntity<PostDto[]> resp = restTemplate.exchange(url, HttpMethod.GET, entity, PostDto[].class);
      return resp.getBody();
   }

   public void Post(String authorization, PostDto dto) {
      RestTemplate restTemplate = new RestTemplate();
      String url = "http://localhost:8080/posts";
      HttpHeaders headers1 = new HttpHeaders();
      headers1.add("Authorization", authorization);
      headers1.add("Content-Type", "application/json");
      HttpEntity<PostDto> entity = new HttpEntity<>(dto, headers1);
      restTemplate.exchange(url, HttpMethod.POST, entity, String.class);
   }

   public void patch(String authorization, PatchPostDto dto, String postId) {
      RestTemplate restTemplate = new RestTemplate(new HttpComponentsClientHttpRequestFactory());
      String url = "http://localhost:8080/posts/" + postId;
      HttpHeaders headers1 = new HttpHeaders();
      headers1.add("Authorization", authorization);
      headers1.add("Content-Type", "application/json");
      HttpEntity<PatchPostDto> entity = new HttpEntity<>(dto, headers1);
      restTemplate.exchange(url, HttpMethod.PATCH, entity, String.class);
   }

   public void sharePost(String authorization, String userId, String postId) {
      RestTemplate restTemplate = new RestTemplate();
      String url = "http://localhost:8080/share";
      HttpHeaders headers1 = new HttpHeaders();
      headers1.add("Authorization", authorization);
      headers1.add("Content-Type", "application/json");
      HttpEntity<SharePostDto> entity = new HttpEntity<>(new SharePostDto(userId, postId), headers1);
      restTemplate.exchange(url, HttpMethod.POST, entity, String.class);
   }

   public void removePost(String authorization, String postId) {
      RestTemplate restTemplate = new RestTemplate();
      String url = "http://localhost:8080/posts/" + postId;
      HttpHeaders headers1 = new HttpHeaders();
      headers1.add("Authorization", authorization);
      headers1.add("Content-Type", "application/json");
      HttpEntity<SharePostDto> entity = new HttpEntity<>(headers1);
      restTemplate.exchange(url, HttpMethod.DELETE, entity, String.class);
   }
}
