package ashung.fakelife.infrastructure;

import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestTemplate;

import ashung.fakelife.model.RelationDto;

public class RelationsManager {

   public RelationDto[] getAll(String authorization, String userId) {
      RestTemplate restTemplate = new RestTemplate();
      String url = "http://localhost:8080/relation/" + userId;
      HttpHeaders headers1 = new HttpHeaders();
      headers1.add("Authorization", authorization);
      HttpEntity<String> entity = new HttpEntity<>(headers1);
      ResponseEntity<RelationDto[]> resp = restTemplate.exchange(url, HttpMethod.GET, entity, RelationDto[].class);
      return resp.getBody();
   }

   public void newRelation(String authorization, RelationDto relationDto) {
      RestTemplate restTemplate = new RestTemplate();
      String url = "http://localhost:8080/relation";
      HttpHeaders headers1 = new HttpHeaders();
      headers1.add("Authorization", authorization);
      HttpEntity<RelationDto> entity = new HttpEntity<>(relationDto, headers1);
      restTemplate.exchange(url, HttpMethod.POST, entity, RelationDto[].class);
   }

}
