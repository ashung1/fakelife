package ashung.fakelife.infrastructure;

import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestTemplate;

import ashung.fakelife.model.UserDto;

public class UserManager {

   public UserDto get(String authorization, String userId) {
      RestTemplate restTemplate = new RestTemplate();
      String url = "http://localhost:8080/user/" + userId;
      HttpHeaders headers1 = new HttpHeaders();
      headers1.add("Authorization", authorization);
      HttpEntity<String> entity = new HttpEntity<>(headers1);
      ResponseEntity<UserDto> resp = restTemplate.exchange(url, HttpMethod.GET, entity, UserDto.class);
      return resp.getBody();
   }

   public UserDto addUser(String authorization, UserDto userDto) {
      RestTemplate restTemplate = new RestTemplate();
      String url = "http://localhost:8080/user";
      HttpHeaders headers1 = new HttpHeaders();
      headers1.add("Authorization", authorization);
      HttpEntity<UserDto> entity = new HttpEntity<>(userDto, headers1);
      return restTemplate.exchange(url, HttpMethod.POST, entity, UserDto.class).getBody();
   }

}
