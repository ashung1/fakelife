package ashung.fakelife.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Scope;

import ashung.fakelife.infrastructure.PostsManager;
import ashung.fakelife.infrastructure.RelationsManager;
import ashung.fakelife.infrastructure.UserManager;

@Configuration
public class ResourceConfig {

   @Bean(name = "postsManager")
   @Scope("singleton")
   public PostsManager postsManager() {
      return new PostsManager();
   }

   @Bean(name = "relationsManager")
   @Scope("singleton")
   public RelationsManager relationsManager() {
      return new RelationsManager();
   }

   @Bean(name = "userManager")
   @Scope("singleton")
   public UserManager userManager() {
      return new UserManager();
   }
}
