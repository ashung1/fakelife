package ashung.fakelife.controllers;

import javax.annotation.Resource;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RestController;

import ashung.fakelife.infrastructure.RelationsManager;
import ashung.fakelife.model.RelationDto;

@RestController
public class RelationController {
   @Resource(name = "relationsManager")
   private RelationsManager relationsManager;

   @GetMapping(value = "/relation/{userId}", produces = "application/json")
   public RelationDto[] get(@PathVariable("userId") String userId,
         @RequestHeader("Authorization") String authorization) {
      return relationsManager.getAll(authorization, userId);
   }

   @PostMapping(value = "/relation", consumes = "application/json")
   public void post(@RequestHeader("Authorization") String authorization, @RequestBody RelationDto relationDto) {
      relationsManager.newRelation(authorization, relationDto);
   }

}
