package ashung.fakelife.controllers;

import java.io.IOException;

import javax.annotation.Resource;

import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RestController;

import ashung.fakelife.infrastructure.PostsManager;
import ashung.fakelife.model.PatchPostDto;
import ashung.fakelife.model.PostDto;
import ashung.fakelife.model.SharePostDto;

@RestController
public class PostController {
   @Resource(name = "postsManager")
   private PostsManager postsManager;

   @GetMapping(value = "/allposts/{userId}", produces = "application/json")
   public PostDto[] getPosts(@PathVariable("userId") String userId,
         @RequestHeader("Authorization") String authorization) throws IOException {
      return postsManager.getPosts(authorization, userId);
   }

   @PostMapping(value = "/posts", produces = "application/json")
   public void post(@RequestHeader("Authorization") String authorization, @RequestBody PostDto postDto)
         throws IOException {
      postsManager.Post(authorization, postDto);
   }

   @PatchMapping(value = "/posts/{postId}", consumes = "application/json")
   public void patch(@PathVariable("postId") String postId, @RequestHeader("Authorization") String authorization,
         @RequestBody PatchPostDto patchPostDto) {
      postsManager.patch(authorization, patchPostDto, postId);
   }

   @PostMapping(value = "/share", consumes = "application/json")
   public void share(@RequestHeader("Authorization") String authorization, @RequestBody SharePostDto sharePostDto) {
      postsManager.sharePost(authorization, sharePostDto.getUserId(), sharePostDto.getPostId());
   }

   @DeleteMapping(value = "/posts/{postId}")
   public void post(@PathVariable("postId") String postId, @RequestHeader("Authorization") String authorization) {
      postsManager.removePost(authorization, postId);
   }
}
