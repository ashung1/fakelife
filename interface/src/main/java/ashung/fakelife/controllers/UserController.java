package ashung.fakelife.controllers;

import javax.annotation.Resource;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RestController;

import ashung.fakelife.infrastructure.UserManager;
import ashung.fakelife.model.UserDto;

@RestController
public class UserController {
   @Resource(name = "userManager")
   private UserManager userManager;

   @GetMapping(value = "/user/{userId}", produces = "application/json")
   public UserDto get(@PathVariable("userId") String userId,
         @RequestHeader("Authorization") String authorization) {
      return userManager.get(authorization, userId);
   }

   @PostMapping(value = "/user", consumes = "application/json", produces = "application/json")
   public UserDto post(@RequestHeader("Authorization") String authorization, @RequestBody UserDto userDto) {
      return userManager.addUser(authorization, userDto);
   }
}
