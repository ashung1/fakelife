package ashung.fakelife;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.nio.charset.Charset;

import org.apache.commons.codec.binary.Base64;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.http.client.HttpComponentsClientHttpRequestFactory;
import org.springframework.web.client.RestTemplate;

import com.fasterxml.jackson.databind.ObjectMapper;

import ashung.fakelife.model.IdentityDto;
import ashung.fakelife.model.PatchPostDto;
import ashung.fakelife.model.PostDto;
import ashung.fakelife.model.RelationDto;
import ashung.fakelife.model.RelationDto.RelationType;
import ashung.fakelife.model.ReplyMessageDto;
import ashung.fakelife.model.SharePostDto;
import ashung.fakelife.model.UserDto;

public class CLIClient {
   static ObjectMapper objectm = new ObjectMapper();
   public static void main(String[] args) {
      try (BufferedReader reader = new BufferedReader(new InputStreamReader(System.in))) {
         String token = getToken();
         System.out.println(token);
         while(true)
         {
            commandline();
            String input = reader.readLine();
            switch (input)
            {
            case "1":
               addUser(token, reader);
               break;
            case "2":
               post(token, reader);
               break;
            case "3":
               newRelation(token, reader);
               break;
            case "4":
               like(token, reader);
               break;
            case "5":
               reply(token, reader);
               break;
            case "6":
               share(token, reader);
               break;
            case "exit":
               return;
               default:
                  System.out.println("Wrong command.");
            }
         }

      } catch (IOException e) {
         e.printStackTrace();
      }
   }
   
   private static void showPosts(String token, String userId)
   {
      System.out.println("[User id]" + "\t" + "[Post content]" + "\t" + "[Like]" +"\t" + "[Post id]");
      System.out.println("[User id]" + "\t" + "[Reply]");
      for(PostDto dto : getPost(token, userId))
      {
         System.out.println(dto.getUserId() + "\t" + dto.getStringContent() + "\t" + dto.getLike().size() +"\t" + dto.getId());
         if (dto.getReplies() != null)
            for (ReplyMessageDto reply : dto.getReplies())
               System.out.println(reply.getUserId() + "\t" +reply.getText());
         System.out.println();
      }
   }
   private static void share(String token, BufferedReader reader) throws IOException {
      SharePostDto sharePostDto = new SharePostDto();
      System.out.print("UserId: ");
      String userId = reader.readLine();
      System.out.print("PostId: ");
      String postId = reader.readLine();
      sharePostDto.setPostId(postId);
      sharePostDto.setUserId(userId);
      RestTemplate restTemplate = new RestTemplate();
      String url = "http://localhost:8090/share";
      HttpHeaders headers1 = new HttpHeaders();
      headers1.add("Authorization", token);
      headers1.add("Content-Type", "application/json");
      HttpEntity<SharePostDto> entity = new HttpEntity<>(sharePostDto, headers1);
      restTemplate.exchange(url, HttpMethod.POST, entity, SharePostDto.class);
      showPosts(token, userId);
   }

   private static void reply(String token, BufferedReader reader) throws IOException {
      PatchPostDto dto = new PatchPostDto();
      dto.setLike(false);
      System.out.print("UserId: ");
      String userId = reader.readLine();
      System.out.print("PostId: ");
      String postId = reader.readLine();
      System.out.print("Reply: ");
      String reply = reader.readLine();
      dto.setUserId(userId);
      dto.setReplyMessage(reply);
      RestTemplate restTemplate = new RestTemplate(new HttpComponentsClientHttpRequestFactory());
      String url = "http://localhost:8090/posts/" + postId;
      HttpHeaders headers1 = new HttpHeaders();
      headers1.add("Authorization", token);
      headers1.add("Content-Type", "application/json");
      HttpEntity<PatchPostDto> entity = new HttpEntity<>(dto, headers1);
      restTemplate.exchange(url, HttpMethod.PATCH, entity, PatchPostDto.class);
      showPosts(token, userId);
   }

   private static void commandline()
   {
      System.out.println("");
      System.out.println("1. Add user");
      System.out.println("2. New post");
      System.out.println("3. Add relation");
      System.out.println("4. Like");
      System.out.println("5. Reply");
      System.out.print("Input: ");
   }
   
   private static PostDto[] getPost(String token, String userId) {
      RestTemplate restTemplate = new RestTemplate();
      String url = "http://localhost:8090/allposts/" + userId;
      HttpHeaders headers1 = new HttpHeaders();
      headers1.add("Authorization", token);
      headers1.add("Content-Type", "application/json");
      HttpEntity<String> entity = new HttpEntity<>(null, headers1);
      return restTemplate.exchange(url, HttpMethod.GET, entity, PostDto[].class).getBody();
   }

   private static void post(String token, BufferedReader reader) throws IOException {
      System.out.print("UserId: ");
      String userId = reader.readLine();
      System.out.print("Post: ");
      String postString = reader.readLine();
      PostDto dto = new PostDto();
      dto.setStringContent(postString);
      dto.setUserId(userId);
      RestTemplate restTemplate = new RestTemplate();
      String url = "http://localhost:8090/posts";
      HttpHeaders headers1 = new HttpHeaders();
      headers1.add("Authorization", token);
      headers1.add("Content-Type", "application/json");
      HttpEntity<PostDto> entity = new HttpEntity<>(dto, headers1);
      System.out.println(objectm.writeValueAsString(dto));
      restTemplate.exchange(url, HttpMethod.POST, entity, UserDto.class);
      showPosts(token, userId);
   }

   private static void like(String token, BufferedReader reader) throws IOException {
      PatchPostDto dto = new PatchPostDto();
      dto.setLike(true);
      System.out.print("UserId: ");
      String userId = reader.readLine();
      System.out.print("PostId: ");
      String postId = reader.readLine();
      dto.setUserId(userId);
      RestTemplate restTemplate = new RestTemplate(new HttpComponentsClientHttpRequestFactory());
      String url = "http://localhost:8090/posts/" + postId;
      HttpHeaders headers1 = new HttpHeaders();
      headers1.add("Authorization", token);
      headers1.add("Content-Type", "application/json");
      HttpEntity<PatchPostDto> entity = new HttpEntity<>(dto, headers1);
      restTemplate.exchange(url, HttpMethod.PATCH, entity, PatchPostDto.class);
      showPosts(token, userId);
   }
   
   private static void newRelation(String token, BufferedReader reader) throws IOException {
      RelationDto dto = new RelationDto();
      System.out.print("From: ");
      String from = reader.readLine();
      System.out.print("To: ");
      String to = reader.readLine();
      dto.setFromId(from);
      dto.setTargetId(to);
      dto.setType(RelationType.Follow);
      RestTemplate restTemplate = new RestTemplate();
      String url = "http://localhost:8090/relation";
      HttpHeaders headers1 = new HttpHeaders();
      headers1.add("Authorization", token);
      headers1.add("Content-Type", "application/json");
      HttpEntity<RelationDto> entity = new HttpEntity<>(dto, headers1);
      restTemplate.exchange(url, HttpMethod.POST, entity, RelationDto.class);
      showPosts(token, from);
   }
   
   private static void addUser(String token, BufferedReader reader) throws IOException {
      System.out.print("UserName:");
      String userName = reader.readLine();
      UserDto userDto = new UserDto();
      userDto.setDisplayName(userName);
      IdentityDto identityDto = new IdentityDto();
      identityDto.setName(userName);
      identityDto.setId("A123456789");
      userDto.setIdentity(identityDto);
      RestTemplate restTemplate = new RestTemplate();
      String url = "http://localhost:8090/user";
      HttpHeaders headers1 = new HttpHeaders();
      headers1.add("Authorization", token);
      headers1.add("Content-Type", "application/json");
      HttpEntity<UserDto> entity = new HttpEntity<>(userDto, headers1);
      System.out.println(objectm.writeValueAsString(userDto));
      String id = restTemplate.exchange(url, HttpMethod.POST, entity, UserDto.class).getBody().getId();
      System.out.println("new user id: " + id);
      showPosts(token, id);
   }

   static HttpHeaders createHeaders(String username, String password) {
      return new HttpHeaders() {
         {
            String auth = username + ":" + password;
            byte[] encodedAuth = Base64.encodeBase64(auth.getBytes(Charset.forName("UTF-8")));
            String authHeader = "Basic " + new String(encodedAuth);
            System.out.println(authHeader);
            set("Authorization", authHeader);
         }
      };
   }

   static String getToken() {
      RestTemplate restTemplate = new RestTemplate();
      String url = "http://127.0.0.1:8080/oauth/authorize?response_type=token&client_id=fakelife-client&scope=read&redirect_uri=/hello";
      HttpHeaders headers1 = createHeaders("admin", "pass");
      HttpEntity<String> entity = new HttpEntity<>(null, headers1);
      ResponseEntity<String> exchange = restTemplate.exchange(url, HttpMethod.POST, entity, String.class);
      String location = exchange.getHeaders().getLocation().toString();
      return "bearer " + location.substring(location.indexOf("access_token=") + 13).substring(0, 36);
   }
}
